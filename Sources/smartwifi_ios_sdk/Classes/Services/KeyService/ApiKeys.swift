//
//  ApiKeys.swift
//  
//
//  Created by Anton Bespalov on 25.10.2023.
//

import Foundation

struct ApiKeys: Codable {
    let password: String?
}
