//
//  KeyService.swift
//  
//
//  Created by Anton Bespalov on 25.10.2023.
//

import CryptoKit
import Foundation

final class KeyService {
    
    static func password() -> String? {
        guard let apiKeys = decryptKeys() else { return nil }
        return apiKeys.password
    }
}

private extension KeyService {
    
    static func saltKey() -> String {
        let classes = [
            String(describing: SWFSecuredStorageManager.self),
            String(describing: SWFUserDefaultsManager.self),
        ]
        let saltKey = classes.joined()
        return saltKey
    }

    static func decryptKeys() -> ApiKeys? {
        guard let filepath = Bundle.module.url(forResource: "keys", withExtension: "txt") else { return nil }
        do {
            let content = try String(contentsOf: filepath)
            let contentBase64Data = content.data(using: .utf8)!
            
            guard let contentData = Data(base64Encoded: contentBase64Data) else { return nil }
            
            let saltKey = saltKey()
            let cryptKey = try SymmetricKey(string: saltKey)

            let decryptedData = try decryptData(ciphertext: contentData, key: cryptKey)
            guard let decryptedString = String(data: decryptedData, encoding: .utf8) else { return nil }
            
            if let apiKeys = decryptedString.toApiKeys() {
                return apiKeys
            }
        } catch {
            print("error = \(error)")
        }
        return nil
    }
  
    static func encryptData(data: Data, key: SymmetricKey) throws -> Data {
        let sealedBox = try AES.GCM.seal(data, using: key)
        return sealedBox.combined!
    }

    static func decryptData(ciphertext: Data, key: SymmetricKey) throws -> Data {
        let sealedBox = try AES.GCM.SealedBox(combined: ciphertext)
        return try AES.GCM.open(sealedBox, using: key)
    }
}

private extension SymmetricKey {
    init(string keyString: String, size: SymmetricKeySize = .bits256) throws {
        guard var keyData = keyString.data(using: .utf8) else {
            print("Could not create base64 encoded Data from String.")
            throw CryptoKitError.incorrectParameterSize
        }
        
        let keySizeBytes = size.bitCount / 8
        keyData = keyData.subdata(in: 0..<keySizeBytes)
        
        guard keyData.count >= keySizeBytes else { throw CryptoKitError.incorrectKeySize }
        self.init(data: keyData)
    }
}

private extension ApiKeys {
    func toJsonString() -> String? {
        do {
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(self)
            let jsonString = String(data: jsonData, encoding: .utf8)
            return jsonString
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
}

private extension String {
    func toApiKeys() -> ApiKeys? {
        do {
            let decoder = JSONDecoder()
            let jsonData = Data(self.utf8)
            let apiKeys = try decoder.decode(ApiKeys.self, from: jsonData)
            return apiKeys
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
}
