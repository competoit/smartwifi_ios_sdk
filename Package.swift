// swift-tools-version: 5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "smartwifi_ios_sdk",
    defaultLocalization: "en",
    platforms: [.iOS(.v14)],
    products: [
        .library(
            name: "smartwifi_ios_sdk",
            targets: ["smartwifi_ios_sdk"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "smartwifi_ios_sdk",
            dependencies: [],
            resources: [
                .copy("Classes/Services/KeyService/keys.txt")
            ]
        ),
        .testTarget(
            name: "smartwifi_ios_sdkTests",
            dependencies: ["smartwifi_ios_sdk"]),
    ]
)
